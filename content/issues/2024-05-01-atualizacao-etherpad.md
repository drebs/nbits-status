---
title: Manutenção programada
date: 2024-05-01 09:00:00
informational: true
pin: true 
severity: notice
affected:
  - pad.eativismo.org
section: issue
---

No dia 01/05/24 haverá breve indisponibilidade do serviço para migração e
atualização.
